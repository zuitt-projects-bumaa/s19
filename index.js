//alert("aftie!")

// ES6 Updates
	// EcmaScript6 or ES6 - newest version of JS
	// JS is formerly known as EcmaScript
	// ECMA- European Computer Manufacturers Association Script

// mini-activity


//Template literals are part of JS ES6 Updates
	/*
		``  - backticks
		${} - placeholder

	*/
let string1 = "Zuitt";
let string2 = "Coding";
let string3 = "Bootcamp";
let string4 = "teaches";
let string5 = "javascript";

let sentence = (`${string1} ${string2} ${string3} ${string4} ${string5}.`)

console.log(sentence);


// EXPONENT OPERATOR (**) instead of Math.pow

let fivePowerOfThree = Math.pow(5, 3);

console.log(fivePowerOfThree);

let fivePowerOfTwo = 5 ** 2;
console.log(fivePowerOfTwo);


// Template Literals with JS Expression

let sentence2 = `The result of five to the power of two is ${5**2}`
console.log(sentence2);

// Array Destructuring - allows us to save array items in a variable

let array = ["Kobe", "Lebron", "Shaq", "Westbrook"];

console.log(array[2]);

let lakePlayer1 = array[3];
let lakePlayer2 = array[0];

//destructuring --- to easily assign value to different variables in one statement
		// - ORDER MATTERS

const [ , , , westbrook] = array;

/*
console.log(kobe);
	// Kobe
console.log(lebron);
	// Lebron
console.log(shaq);
	// Shaq
*/

console.log(westbrook);

// mini-activity 

let array2 = ["Curry", "Lillard", "Paul", "Irving"];

/*
instead of:
let pointGuard1 = array[0];
let pointGuard2 = array[1];
let pointGuard3 = array[2];
let pointGuard4 = array[3];
*/

const [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = array2;
 console.log(pointGuard1);
 console.log(pointGuard2);
 console.log(pointGuard3);
 console.log(pointGuard4);

 let bandMembers = ["Hayley", "Zac", "Jeremy", "Taylor"];

 const [vocals, lead,  , bass] = bandMembers;
 console.log(vocals);
 console.log(lead);
 console.log(bass);


// OBJECT DESTRUCTURING
		// -order does not matter

let person = {
	name: "Jeremy Davis",
	birthday: "September 12, 1989", 
	age: 32,

}

let sentence3 = `Hi I am ${person.name}`;
console.log(sentence3);

const {age, firstName, birthday} = person 

console.log(age);
	// 32
console.log(firstName);
	// undefined
console.log(birthday);
	// September 12, 1989


let pokemon1 = {
	name: "Charmander",
	level: 11, 
	type: "fire", 
	moves: ["Ember", "Scratch", "Leer"]
}; 

//mini activity 

const {name, level, type, moves} = pokemon1;

let sentence4 = `My pokemon is ${name}, it is in level ${level}. It is a ${type}. It's moves are ${moves}.`

console.log(sentence4);



// ARROW FUNCTIONS
	// -ALTERNATIVE WAY OF WRITING FUNCTIONS
	// -have significant pros and cons 
	

// traditional:
function displayMsg () {
	console.log("Hello World!"); 
}


// arrow function: 
const hello = () => {
	console.log("Hello World!");
};

hello();


const greet = (person) => {
	console.log(`Hi ${person.name}`)
};

greet(person); 

// Adv: Implicit return --no need for return keyword and {} for 1-liner
const addNum = (num1, num2) => num1 + num2 

let sum = addNum(55,60);
console.log(sum);

// this keyword 

let protagonist = {
	name: "Cloud Strife",
	occupation: "SOLDIER",
	// traditional method would have this keyword 
	greet: () => {
		console.log(this);
		console.log(`Hi, I am ${this.name};`)
	},
	introduce: () => {
		console.log(this);
		console.log(`I work as ${this.occupation}`)
	}
};

// disadv: arrow method this- refers to global
protagonist.greet();
protagonist.introduce();


// Class-Base Objects Blueprints
	// In Javascript, classes are templates of objects
	// We can create objects out of the use of classes

function Pokemon(name, type, lvl) {
	this.name = name;
	this.type = type; 
	this.lvl = lvl;
};

// ES6 update: Class Creation
class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name; 
		this.year = year;
	}
};

let car1 = new Car("Toyota", "Vios", "2002");
console.log(car1);

let car2 = new Car("Cooper","Mini","1969");
console.log(car2);


























